# Data Science - Crime in Estonia
Registered crimes commited in public places 2014 - 2019


This project was done by team T5.

Main project code file is project.ipynb.

We gathered our data from: https://opendata.riik.ee/andmehulgad/avaliku-korra-vastased-ja-avalikus-kohas-toime-pandud-syyteod/ and https://www.stat.ee/ppe-45400

Our tasks were:

1.	Merge the two databases with one having data for 2014-2018 and the other having 2018-2019. That means also not replicating the 2018 that they both share.

2.	Clean up the data. The data needs to be checked further, checking for empty rows or faulty parts and removing columns that wouldn�t be needed for our research.

3.	Make general evaluations. See what the data can tell us about simple distributions, like where the bigger portion of crime is committed or what are the most prevalent crimes.

4.	Find if there are correlations between data instances. For example, related to either when more crime happens on different weekdays or time of day.

5.	Making heatmaps for different variables on the map of Estonia. General one of where most crime happens or for different variations of crime, for example where most muggings or robberies happen.

6.	Gather data on populations of the counties of Estonia and get better results on occurrence of crime based per capita for each county, showing where crime is more occurrent when taking population into account.

7.	Create a poster that gives an overview and summary of the project, including heatmaps and diagrams of the gathered and processed data.

You can read more about the project in T5_report.pdf.

